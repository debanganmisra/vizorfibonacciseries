﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VizorFibo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the limit(nth term) upto which you want the fibonacci series to run");
            int limit=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("The nth number in the fibonacci series is::: "+CalculateFibonacci(limit));
            Console.Read();
        }

        /// <summary>
        /// Function to calculate fibonacci
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static int CalculateFibonacci(int limit)
        {
            int n1 = 0;
            int n2 = 1;

            int result=0;
            for(int i=0;i<limit;i++)
            {
                if (i > 1)
                {
                    result = n1 + n2;
                    n1 = n2;
                    n2 = result;
                }
                else if(i==0 || i==1)
                {
                    result = i;
                }
            }
            return result;
        }
    }
}
